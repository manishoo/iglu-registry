# Minds Iglu Schema Registry

This repository houses the iglue schemas for the Minds Snowplow implementation.

## Generate DDLs and Migrations

`./igluctl static generate schemas`

## Setup env vars

```
export IGLU_SERVER=a70f26ec7c7604bc4a28e1c67f45d88e-36677981.us-east-1.elb.amazonaws.com

export IGLU_API_KEY=YOUR_API_KEY_FOR_IGLU
```

## Copy schemas

```
./igluctl static push schemas $IGLU_SERVER $IGLU_API_KEY
```